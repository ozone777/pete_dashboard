<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |{{trans('dashboard.my_account') }}
    */
	'my_subscriptions' => 'My subscriptions',
	'need_help' => 'Need help?',
	
];