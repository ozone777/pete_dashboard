@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h3>
            Options
            <a class="btn btn-success pull-right" href="{{ route('wp_global_options.create') }}">Create</a>
        </h3>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($wp_global_options->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Option name</th>
							<th>Option value</th>
							<th>Option image</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($wp_global_options as $wp_global_option)
                            <tr>
                                <td>{{$wp_global_option->id}}</td>
                                <td>{{$wp_global_option->option_name}}</td>
								<td>{{$wp_global_option->option_value}}</td>
								<td>
									@if($wp_global_option->image)
										<img class="img-responsive" src="/options/{{$wp_global_option->image}}">
									@endif
								
								</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('wp_global_options.show', $wp_global_option->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('wp_global_options.edit', $wp_global_option->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('wp_global_options.destroy', $wp_global_option->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $wp_global_options->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection