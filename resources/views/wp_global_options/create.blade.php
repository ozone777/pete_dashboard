@extends('layout')

@section('header')
    <div class="page-header">
        <h3> Options / Create </h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">
			
            <form action="{{ route('wp_global_options.store') }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('meta_key')) has-error @endif">
                       <label for="meta_key-field">Option name</label>
                    <input type="text" id="option_name-field" name="option_name" class="form-control" value="{{ old("option_name") }}"/>
                      
                    </div>
                    <div class="form-group @if($errors->has('meta_value')) has-error @endif">
                       <label for="meta_value-field">Option value</label>
                    <textarea class="form-control" id="option_value-field" rows="3" name="option_value">{{ old("option_value") }}</textarea>
                       
                    </div>
					
						
		            <div class="form-group">
		              <label for="zip_file_url-field">Image</label>
	                  <input type="file" id="image" name="image">
		            </div>
				
				 	
					
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
					 <a class="btn btn-link pull-right" href="{{ route('wp_global_options.index') }}">Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection