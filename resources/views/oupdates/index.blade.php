@extends('layout')

@section('header')
    
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
			
		   
				
				<a class="btn btn-success pull-right" href="{{ route('oupdates.create') }}">Create</a>
		        <h3>Oupdates</h3>
		            	
		   
			
            @if($oupdates->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            
							<th>Parent Version</th>
							<th>Version</th>
                            <th>Scripts</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($oupdates as $oupdate)
                            <tr>
                                
								<td>{{$oupdate->parent_version}}</td>
								<td>{{$oupdate->version}}</td>
                                <td>
									{{$oupdate->mac_script}} <br />
									{{$oupdate->olinux_script}}<br />
									{{$oupdate->win_script}}
								</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('oupdates.show', $oupdate->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('oupdates.edit', $oupdate->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('oupdates.destroy', $oupdate->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $oupdates->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection