@extends('layout')

@section('header')
    
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">
			
		    <div class="page-header">
		        <h3>Oupdates / Edit #{{$oupdate->id}}</h3>
		    </div>

            <form action="{{ route('oupdates.update', $oupdate->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

				@include('oupdates._form')
                
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('oupdates.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection