<label for="meta_value-field">Parent Version</label>
<input type="text" id="parent_version" name="parent_version" class="form-control" value="{{$oupdate->parent_version}}"/>
               
<label for="meta_value-field">Version</label>
<input type="text" id="version" name="version" class="form-control" value="{{$oupdate->version}}"/>
				
<label for="meta_value-field">Mac Script</label>
<textarea class="form-control" id="mac_script" rows="7" name="mac_script">{{$oupdate->mac_script}}</textarea>
				
<label for="meta_value-field">Linux Script</label>
<textarea class="form-control" id="olinux_script" rows="7" name="olinux_script">{{$oupdate->olinux_script}}</textarea>
				
<label for="meta_value-field">Win Script</label>
 <textarea class="form-control" id="win_script" rows="7" name="win_script">{{$oupdate->win_script}}</textarea>