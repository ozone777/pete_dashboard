<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Pete Dashboard</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>

    <!--  GrowthTroop CSS    -->
   

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	 <link href="/assets/css/growthtroop.css" rel="stylesheet"/>
	
	<style>
		
		.cssload-loader{
			display: inline;
			position:absolute;
			height:3em;width:3em;
			left:50%;
			margin-top:1.2em;
			margin-left:-1em;
			background-color:rgb(10,36,153);
			border-radius:2.5em 2.5em 2.5em 2.5em;
				-o-border-radius:2.5em 2.5em 2.5em 2.5em;
				-ms-border-radius:2.5em 2.5em 2.5em 2.5em;
				-webkit-border-radius:2.5em 2.5em 2.5em 2.5em;
				-moz-border-radius:2.5em 2.5em 2.5em 2.5em;
			box-shadow:inset 0 0 0 0.5em rgb(154,202,60);
				-o-box-shadow:inset 0 0 0 0.5em rgb(154,202,60);
				-ms-box-shadow:inset 0 0 0 0.5em rgb(154,202,60);
				-webkit-box-shadow:inset 0 0 0 0.5em rgb(154,202,60);
				-moz-box-shadow:inset 0 0 0 0.5em rgb(154,202,60);
			background: linear-gradient(-45deg, rgb(60,188,169), rgb(60,188,169) 50%, rgb(2,172,155) 50%, rgb(2,172,155));
				background: -o-linear-gradient(-45deg, rgb(60,188,169), rgb(60,188,169) 50%, rgb(2,172,155) 50%, rgb(2,172,155));
				background: -ms-linear-gradient(-45deg, rgb(60,188,169), rgb(60,188,169) 50%, rgb(2,172,155) 50%, rgb(2,172,155));
				background: -webkit-linear-gradient(-45deg, rgb(60,188,169), rgb(60,188,169) 50%, rgb(2,172,155) 50%, rgb(2,172,155));
				background: -moz-linear-gradient(-45deg, rgb(60,188,169), rgb(60,188,169) 50%, rgb(2,172,155) 50%, rgb(2,172,155));
			background-blend-mode: multiply;
			border-top:5px solid rgb(145,145,145);
			border-left:5px solid rgb(145,145,145);
			border-bottom:5px solid rgb(145,145,145);
			border-right:5px solid rgb(145,145,145);
			animation:cssload-roto 1.15s infinite linear;
				-o-animation:cssload-roto 1.15s infinite linear;
				-ms-animation:cssload-roto 1.15s infinite linear;
				-webkit-animation:cssload-roto 1.15s infinite linear;
				-moz-animation:cssload-roto 1.15s infinite linear;
		}


		@keyframes cssload-roto {
			0%{transform:rotateZ(0deg);}
			100%{transform:rotateZ(360deg);}
		}

		@-o-keyframes cssload-roto {
			0%{-o-transform:rotateZ(0deg);}
			100%{-o-transform:rotateZ(360deg);}
		}

		@-ms-keyframes cssload-roto {
			0%{-ms-transform:rotateZ(0deg);}
			100%{-ms-transform:rotateZ(360deg);}
		}

		@-webkit-keyframes cssload-roto {
			0%{-webkit-transform:rotateZ(0deg);}
			100%{-webkit-transform:rotateZ(360deg);}
		}

		@-moz-keyframes cssload-roto {
			0%{-moz-transform:rotateZ(0deg);}
			100%{-moz-transform:rotateZ(360deg);}
		}
		
		.et_manage_submit {
		    display: block;
		    text-align: center;
		    box-shadow: 0px 5px 20px #d6dee4;
		    -moz-box-shadow: 0px 5px 20px #d6dee4;
		    -webkit-box-shadow: 0px 5px 20px #d6dee4;
		    margin-top: 30px;
		    color: #fff;
		    font-size: 18px;
		    font-weight: 500;
		    padding: 20px;
		    background-color: #85b134;
		    border: none;
		    cursor: pointer;
		    width: 100%;
		    border-radius: 60px;
		    -moz-border-radius: 60px;
		    -webkit-border-radius: 60px;
		    -webkit-transition: all .5s ease;
		    transition: all .5s ease;
		    box-sizing: border-box;
		    -moz-box-sizing: border-box;
		    -webkit-box-sizing: border-box;
		}
		
		.content code {
		    color: #20292f;
		    padding-bottom: 10px;
		    display: block;
		    font-size: 20px;
			background-color: none;
			
		}
		
	</style>

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="{{$dashboard_option->get_meta_value('sidebar_color')}}" data-image="/assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="/" class="simple-text">
               
				<img class="img-responsive" src="/options/{{$dashboard_option->get_meta_image('sidebar_header_image')}}">
                </a>
            </div>

            <ul class="nav">
				
				
				
				@if(($role == "administrator") || ($role == "Contributor"))
				
		
                <li @if($viewsw == "open_tickets") class="active" @endif>
                    <a href="/open_tickets">
                        <i class="pe-7s-headphones"></i>
                        <p>Open tickets</p>
                    </a>
                </li>
				
                <li @if($viewsw == "customers") class="active" @endif>
                    <a href="/customers">
                        <i class="pe-7s-smile"></i>
                        <p>customers</p>
                    </a>
                </li>
				
                 <li @if($viewsw == "plugins") class="active" @endif>
                    <a href="/oupdates">
                        <i class="pe-7s-joy"></i>
                        <p>System Versions</p>
                    </a>
                </li>
				
                 <li @if($viewsw == "plugins") class="active" @endif>
                    <a href="/pete_plugins">
                        <i class="pe-7s-joy"></i>
                        <p>Plugins</p>
                    </a>
                </li>
				
				
                 <li @if($viewsw == "options") class="active" @endif>
                    <a href="/wp_global_options">
                        <i class="pe-7s-joy"></i>
                        <p>Options</p>
                    </a>
                </li>
				
				@else
				
				
                <li @if($viewsw == "my_subscriptions") class="active" @endif>
                    <a href="/my_subscriptions">
                        <i class="pe-7s-user"></i>
                        <p> {{trans('dashboard.my_subscriptions')}}</p>
                    </a>
                </li>
				
                 <li @if($viewsw == "support") class="active" @endif>
                    <a href="/otickets">
                        <i class="pe-7s-headphones"></i>
                        <p>{{trans('dashboard.need_help')}}</p>
                    </a>
                </li>
				
				@endif
				
				
				<div id="loading_area">
				
				</div>
				
               
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                       
                    </ul>

                    <ul class="nav navbar-nav navbar-right">

						
                        <li>
                            <a href="#">
                                {{$current_user->user_email}}
                            </a>
                        </li>
						
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Languages
                                    <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li id ="englishbtn"><a href="#">English</a></li>
                                <li id ="spanishbtn"><a href="#">Español</a></li>
                              </ul>
                        </li>
                    	
                        <li>
                            <a href="/logout">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">


                <div class="row">

                    <div class="col-md-12">
						
						
			        @yield('header')
			        @yield('content')
						
				
		            </div>
		        </div>

            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                
                <p class="copyright pull-right">
                   {{$dashboard_option->get_meta_value("footer_copy")}}
                </p>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
    <!--  Notifications Plugin    -->
    <script src="/assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin   
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    -->
    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="/assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="/assets/js/demo.js"></script>

	<script type="text/javascript">
    	$(document).ready(function(){

			@if($success == "yes")
        	
			
        	$.notify({
            	icon: 'pe-7s-arc',
            	message: "API Key Successfully Created"

            },{
                type: 'info',
                timer: 4000
            });
			
			@elseif($success == "no")
			
        	$.notify({
            	icon: 'pe-7s-arc',
            	message: "Sorry you need to upgrade your license"

            },{
                type: 'info',
                timer: 4000
            });
			
			@endif
			
			
			@if($message)
        	
        	$.notify({
            	icon: 'pe-7s-arc',
            	message: "{{$message}}"

            },{
                type: 'info',
                timer: 4000
            });
			
			@endif
			
			
			$( "#spanishbtn" ).click(function() {
				
				$("#loading_area").prepend('<div class="cssload-loader"></div>');
				
		        $.ajax({
		         type: "POST",
		         url: "/set_lang",
		         dataType: "json",
		         data: { lang: "es", _token: "{{ csrf_token() }}"},
		         success: function(data){
					location.reload();
		         }
		       });
			});
			
			$( "#englishbtn" ).click(function() {
				
				$("#loading_area").prepend('<div class="cssload-loader"></div>');
				
		        $.ajax({
		         type: "POST",
		         url: "/set_lang",
		         dataType: "json",
		         data: { lang: "en", _token: "{{ csrf_token() }}"},
		         success: function(data){
					 location.reload();
		         }
		       });
			});
			
    	});
	</script>


	<script type="text/javascript">
	
	$( document ).ajaxStart(function() {
		
		if($(".cssload-loader").length == 0){
			
	      $("#loading_area").append('<div class="cssload-loader"></div>');
		}
		
	});
	
	$(document).ajaxSuccess(function() {
	  $(".cssload-loader" ).remove();
	  
	});

	</script>

</html>
