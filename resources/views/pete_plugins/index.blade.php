@extends('layout')

@section('header')

<style>
	code{
		font-size: 10px !important;
	}
</style>
   
@endsection

@section('content')

   <div class="page-header clearfix">
       <h3>
           PetePlugins
           <a class="btn btn-success pull-right" href="{{ route('pete_plugins.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
       </h3>

   </div>

    <div class="row">
        <div class="col-md-12">
            @if($pete_plugins->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Version</th>
                            <th>Title</th>
							<th>Image</th>
							<th>Description</th>
							<th>Script</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($pete_plugins as $pete_plugin)
                            <tr>
                                <td>{{$pete_plugin->version}}</td>
								
								<td>{{$pete_plugin->title}}</td>
								<td>
								@if($pete_plugin->image)
									<img class="img-responsive" src="/plugins_images/{{$pete_plugin->image}}">
								@endif
								</td>
								<td>{{$pete_plugin->description}}</td>
								<td>
									<h3>MAC</h3>
									<p>Install script</p>
									<code>{{$pete_plugin->mac_install_script}}</code>
									
									<p>Update script</p>
									<code>{{$pete_plugin->mac_update_script}}</code>
									
									<p>Uninstall script</p>
									<code>{{$pete_plugin->mac_uninstall_script}}</code>
									
									<h3>OLINUX</h3>
									<p>Install script</p>
									<code>{{$pete_plugin->olinux_install_script}}</code>
									
									<p>Update script</p>
									<code>{{$pete_plugin->olinux_update_script}}</code>
									
									<p>Uninstall script</p>
									<code>{{$pete_plugin->olinux_uninstall_script}}</code>
									
									
									<h3>WIN</h3>
									<p>Install script</p>
									<code>{{$pete_plugin->win_install_script}}</code>
									
									<p>Update script</p>
									<code>{{$pete_plugin->win_update_script}}</code>
									
									<p>Uninstall script</p>
									<code>{{$pete_plugin->win_uninstall_script}}</code>
									
								</td>
								
                                
                               <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('pete_plugins.show', $pete_plugin->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('pete_plugins.edit', $pete_plugin->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('pete_plugins.destroy', $pete_plugin->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $pete_plugins->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection