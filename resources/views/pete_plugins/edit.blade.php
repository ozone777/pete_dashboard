@extends('layout')

@section('header')
    <div class="page-header">
        <h3>PetePlugins / Edit #{{$pete_plugin->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('pete_plugins.update', $pete_plugin->id) }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

  		  		<label for="meta_value-field">Title</label>
                <input type="text" id="title" name="title" class="form-control" value="{{$pete_plugin->title}}"/>
				
  		  		<label for="meta_value-field">Version</label>
                <input type="text" id="version" name="version" class="form-control" value="{{$pete_plugin->version}}"/>
				      
                <label for="meta_value-field">Description</label>
                <textarea class="form-control" id="description" rows="7" name="description">{{$pete_plugin->description}}</textarea>
				
				<h3>MAC</h3>
				
                <label for="meta_value-field">Install Script</label>
                <textarea class="form-control" id="install_script" rows="7" name="install_script">{{$pete_plugin->mac_install_script}}</textarea>
				
                <label for="meta_value-field">Uninstall Script</label>
                <textarea class="form-control" id="uninstall_script" rows="7" name="uninstall_script">{{$pete_plugin->mac_uninstall_script}}</textarea>
				
                <label for="meta_value-field">Update Script</label>
                <textarea class="form-control" id="update_script" rows="7" name="update_script">{{$pete_plugin->mac_update_script}}</textarea>
				
				
				<h3>OLINUX</h3>
				
                <label for="meta_value-field">Install Script</label>
                <textarea class="form-control" id="install_script" rows="7" name="install_script">{{$pete_plugin->olinux_install_script}}</textarea>
				
                <label for="meta_value-field">Uninstall Script</label>
                <textarea class="form-control" id="uninstall_script" rows="7" name="uninstall_script">{{$pete_plugin->olinux_uninstall_script}}</textarea>
				
                <label for="meta_value-field">Update Script</label>
                <textarea class="form-control" id="update_script" rows="7" name="update_script">{{$pete_plugin->olinux_update_script}}</textarea>
             	
	            <div class="form-group">
	              <label for="zip_file_url-field">Image</label>
                  <input type="file" id="image" name="image">
	            </div>
				
                
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('pete_plugins.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection