##The Global dashboard Ozone project

Wordpresspluslaravel is a simple WordPress implementation with Laravel 5.1.x that only uses seven steps. It leverages all the power of thousands of WordPress developers added to Laravel's performance in an MVC (Model View Controller) software design pattern. This technology is ideal for creating a powerful Laravel dashboard for a WordPress site.

## Contributing

Thank you for considering contributing to the Wordpresspluslaravel project.

## Security Vulnerabilities

If you discover a security vulnerability within the Wordpresspluslaravel project, please send an e-mail to Peter Consuegra at software@ozonegroup.co. All security vulnerabilities will be promptly addressed.

### License

Wordpresspluslaravel is open-source software licensed under the [MIT license](http://opensource.org/licenses/MIT)

# Global dashboard Ozone install instructions

For increment ticket ID from 1000 run this command via sql
ALTER TABLE wp_otickets AUTO_INCREMENT = 1000
SET sql_mode = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

Troubleshoot
Composer/Console error is because the laravel 5 scaffold 5.1 need to replace the src folder



