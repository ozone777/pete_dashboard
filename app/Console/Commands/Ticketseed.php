<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use App\Option;
use App\Oticket;
use App\Oanswer;
use App\User;
use Log;
use App\Standard\LoremIpsum;


class Ticketseed extends Command
{
    /**
     * The console command name.
     * php artisan dbkeys --var=DB_DATABASE --key=pixma303
     * @var string
     */
    protected $signature = 'ticketseed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Support tickets seed';

    /**
     * Execute the console command.
     *
     * @return void
     */
	
    public function __construct()
    {
        parent::__construct();
    }
	
    public function handle()
    {
		
		Oticket::truncate();
		Oanswer::truncate();
		
		$lipsum = new LoremIpsum();
		$status_array = ["OPEN","CLOSED"];
		$users = User::all();
		$users_tam = User::count() - 1;
		
		for ($x = 0; $x <= 20; $x++) {
			
			$user = $users[rand(0, $users_tam)];
		    $status = $status_array[rand(0, 1)];
			
			$int= mt_rand(1262055681,2162055681);
			$opened_by = date("Y-m-d H:i:s",$int);
			
			$int= mt_rand(1262055681,2162055681);
			$updated_by = date("Y-m-d H:i:s",$int);
			
			$oid = mt_rand(500000,900000);
			$ticket_id = "{$user->ID}-{$oid}";
			
			$updated_by_login = $user->user_login;
			
			$oticket = new Oticket();
			$oticket->opened_by = 
			$oticket->summary = $lipsum->words(10);
			$oticket->regarding = $lipsum->words(20);
			$oticket->status = $status;
			$oticket->body = $lipsum->paragraphs(5);
			$oticket->user_id = $user->ID;
			$oticket->updated_by_login = $user->user_login;
			$oticket->opened_by_login = $user->user_login;
			$oticket->updated_by = $updated_by;
			$oticket->opened_by = $opened_by;
			$oticket->ticket_id = $ticket_id;
			$oticket->save();
			
			$tam_answers = rand(0, 5);
			
			for ($j = 0; $j <= $tam_answers; $j++) {
				
				$oanswer = new Oanswer();
				$oanswer->user_id = $user->ID;
				$oanswer->oticket_id = $oticket->id;
				$oanswer->body = $lipsum->paragraphs(3);
				$oanswer->save();
				
			}
			
			
		} 
		
		
    }

    
}
