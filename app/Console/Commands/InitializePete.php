<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;

class InitializePete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	
	//Correr en modo production Esta
	//php artisan initializepete --m production
	
    protected $signature = 'initializepete {--m=}';
	#protected $signature = 'addfields {--queue=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add template records to pete dashboard';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
		DB::table('wp_global_options')->delete();
		
		DB::table('wp_global_options')->insert(['option_name' => 'sidebar_color', 'option_value' => 'azure','created_at' => new DateTime, 'updated_at' => new DateTime]);	
		
		DB::table('wp_global_options')->insert(['option_name' => 'sidebar_header_image', 'option_value' => '','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
		DB::table('wp_global_options')->insert(['option_name' => 'footer_copy', 'option_value' => '© 2018 WordressPete, made with love for a better web','created_at' => new DateTime, 'updated_at' => new DateTime]);		
		
		DB::table('wp_global_options')->insert(['option_name' => 'email_header_image', 'option_value' => '','created_at' => new DateTime, 'updated_at' => new DateTime]);	
		
		DB::table('wp_global_options')->insert(['option_name' => 'sender_email', 'option_value' => 'software@ozonelabs.us','created_at' => new DateTime, 'updated_at' => new DateTime]);	
		
	   DB::table('wp_global_options')->insert(['option_name' => 'default_lang', 'option_value' => 'en','created_at' => new DateTime, 'updated_at' => new DateTime]);
	   
	   DB::table('wp_global_options')->insert(['option_name' => 'system_notification_email', 'option_value' => 'software@ozonelabs.us','created_at' => new DateTime, 'updated_at' => new DateTime]);	
		
		
	 if($this->option('m') == "production"){
	 	
		 DB::table('wp_global_options')->insert(['option_name' => 'dashboard_url', 'option_value' => 'https://dashboard.wordpresspete.com/','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
	 }else{
	 	
		DB::table('wp_global_options')->insert(['option_name' => 'dashboard_url', 'option_value' => 'http://dashboard.petelocal.co/','created_at' => new DateTime, 'updated_at' => new DateTime]);
		
	 }
		
		
		
    }
}
