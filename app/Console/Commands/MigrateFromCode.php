<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class MigrateFromCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate_from_code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a migration from Code';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		
		/*$exitCode = Artisan::call('migrate:refresh', [
		    '--force' => true,
		]);
		*/
			
	    $this->call('migrate', [
		    '--force' => true,
		]);
    }
}
