<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oticket extends Model
{
    //
	protected $table = 'wp_otickets';

	function randomDigits($length){
	    $numbers = range(0,9);
	    shuffle($numbers);
	    for($i = 0; $i < $length; $i++){
	    	global $digits;
	       	$digits .= $numbers[$i];
	    }
	    return $digits;
	}
}
