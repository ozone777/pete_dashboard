<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use View;
use App;
use Log;
use App\DashboardOption;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
		
	   	View::composer('*', function($view){
			 
		  $current_user = Auth::user();			
		  $wordpress_url = env('WP_URL');
		  $dashboard_option = new DashboardOption();
		  $default_lang = $dashboard_option->get_meta_value("default_lang");
		  
		  if(isset($current_user)){
			  //Calling this method from globalfunctions.php
			if(isset($current_user->dashboard_lang)){
		   	  	  App::setLocale($current_user->dashboard_lang);
			  }else{
				  App::setLocale($default_lang);
			}
			  
			 $role = getUserRole($current_user->ID);
		     $view->with(compact('current_user','wordpress_url','role','dashboard_option'));
	      }
		  
	    });
		
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
