<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//API validation routes no login required
Route::get('/validate_site_json', 'OapiController@validate_site_json');
Route::get('/validate_update_json', 'OapiController@validate_update_json');
Route::get('/plugins_json', 'OapiController@plugins_json');
Route::get('/update_button_json', 'OapiController@update_button_json');


//Dashboard routes
Route::get("/my_subscriptions","DashboardController@my_subscriptions");
Route::post("/create_api_key","DashboardController@create_api_key");
Route::get('/logout', 'DashboardController@logout');
Route::post("/set_lang","DashboardController@set_lang");

//Admin routes
Route::get('/open_tickets', 'AdminController@open_tickets');
Route::get('/customers', 'AdminController@customers');
Route::get('/get_customer/{id}', 'AdminController@get_customer');
Route::get('/test_email', 'AdminController@test_email');

//Otickets routes
Route::resource("otickets","OticketController");
Route::post("store_answer","OticketController@store_answer");
Route::post("update_status","OticketController@update_status");
Route::get("all_tickets","OticketController@all");

//Options routes
Route::resource("wp_global_options","WpGlobalOptionController");

Route::resource("pete_plugins","PetePluginController"); // Add this line in routes.php
Route::resource("oupdates","OupdateController");


Route::get('/', array('as' => 'dashboard.my_subscriptions', 'uses' => 'DashboardController@my_subscriptions'));

