<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PetePlugin;
use App\DashboardOption;
use Illuminate\Http\Request;
use View;
use Log;

class PetePluginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct(Request $request){

		$this->middleware('auth.wp');		
		$dashboard_option = new DashboardOption();	
		View::share(compact('dashboard_option'));
	}
	
	
	
	public function index()
	{
		$pete_plugins = PetePlugin::orderBy('id', 'desc')->paginate(10);
		$viewsw = "plugins";
		return view('pete_plugins.index', compact('pete_plugins','viewsw'));
	}
	

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	
		$viewsw = "plugins";
		return view('pete_plugins.create',compact('viewsw '));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$pete_plugin = new PetePlugin();

		$pete_plugin->title = $request->input("title");
		$pete_plugin->version = $request->input("version");
       
        $pete_plugin->description = $request->input("description");
        $pete_plugin->install_script = $request->input("install_script");
		$pete_plugin->uninstall_script = $request->input("uninstall_script");
		$pete_plugin->update_script = $request->input("uninstall_script");
		
		
		
		if($request->file('image')!= ""){
			Log::info('Entro en logica archivo');
				
			$file = $request->file('image');
	        // SET UPLOAD PATH
	        $destinationPath = 'pete_plugins';
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
	        $fileName = rand(11111, 99999) . '.' . $extension;
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$pete_plugin->image = $fileName;
			
		}

		$pete_plugin->save();

		return redirect()->route('pete_plugins.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$pete_plugin = PetePlugin::findOrFail($id);
		$viewsw = "plugins";
		return view('pete_plugins.show', compact('pete_plugin','viewsw'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$pete_plugin = PetePlugin::findOrFail($id);
		$viewsw = "plugins";
		return view('pete_plugins.edit', compact('pete_plugin','viewsw'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$pete_plugin = PetePlugin::findOrFail($id);

		$pete_plugin->title = $request->input("title");
		$pete_plugin->version = $request->input("version");
        $pete_plugin->description = $request->input("description");
        $pete_plugin->install_script = $request->input("install_script");
		$pete_plugin->uninstall_script = $request->input("uninstall_script");
		$pete_plugin->update_script = $request->input("uninstall_script");
		
		if($request->file('image')!= ""){
			Log::info('Entro en logica archivo');
			Log::info($request->file('image'));
			$file = $request->file('image');
	        // SET UPLOAD PATH
	        $destinationPath = 'plugins_images';
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
	        $fileName = rand(11111, 99999) . '.' . $extension;
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$pete_plugin->image = $fileName;
			
		}

		$pete_plugin->save();

		return redirect()->route('pete_plugins.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$pete_plugin = PetePlugin::findOrFail($id);
		$pete_plugin->delete();

		return redirect()->route('pete_plugins.index')->with('message', 'Item deleted successfully.');
	}

}
