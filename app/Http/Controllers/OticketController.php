<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use App\User;
use App\Site;
use App\Oupdate;
use App\Oanswer;
use App\DashboardOption;
use Input;
use Illuminate\Support\Facades\Auth;
use App\Pete;
use App\Oticket;
use Illuminate\Support\Facades\Redirect;
use DB;
use Mailjet;
use View;

class OticketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function __construct()
	{
	     $this->middleware('auth.wp');		
	}
	
    public function all()
    {
		$viewsw = "support";
		$user = Auth::user();
		$otickets = Oticket::where("user_id",$user->ID)->orderBy('status', 'desc')->paginate(10);	
		
		return view('otickets.all',compact('viewsw','otickets'));
    }
	
    public function open_tickets()
    {
		$viewsw = "open_tickets";
		$user = Auth::user();
		$otickets = Oticket::where("status","OPEN")->orderBy('status', 'desc')->paginate(20);	
		
		return view('otickets.all',compact('viewsw','otickets'));
    }
	
    public function index()
    {
		$viewsw = "support";
		$user = Auth::user();
		$open_tickets = Oticket::where("status","OPEN")->where("user_id",$user->ID)->orderBy('created_at', 'desc')->get();
		$closed_tickets = Oticket::where("status","CLOSED")->where("user_id",$user->ID)->orderBy('created_at', 'desc')->get()->take(5);
		
		return view('otickets.index',compact('viewsw','open_tickets','closed_tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$user = Auth::user();
		$viewsw = "support";
		$sites = Site::where("user_id",$user->ID)->where('domain', '!=' , "")->whereNotNull('domain')->get();
		
		return view('otickets.create',compact('viewsw','sites'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$user = Auth::user();
		$ticket = new Oticket();
		$ticket->summary = $request->input("summary");
		$ticket->regarding = $request->input("regarding");
		$ticket->status = "OPEN";
		$ticket->user_id = $user->ID;
		$ticket->opened_by = date('Y-m-d H:i:s');
		$ticket->opened_by_login = $user->user_login;
		$ticket->updated_by = date('Y-m-d H:i:s');
		$ticket->updated_by_login = $user->user_login;
		$oid = $ticket->randomDigits(7);
		$ticket->ticket_id = "{$oid}";
		$ticket->body = $request->input("body");
		$ticket->save();
		
	    $dashboard_option = new DashboardOption();
		$sender_email = $dashboard_option->get_meta_value("sender_email");
		$system_notification_email = $dashboard_option->get_meta_value("system_notification_email");
		
		$params = [
			"method" => "POST",
			"from" => $sender_email,
			"to" => $system_notification_email,
			"subject" => "Importante!! Un suscriptor creo un ticket desde: ".env('WP_URL'),
			"html" => "El usuario". $user->user_email . " creo un ticket desde: ".env('WP_URL')
		];
		
		$result = Mailjet::sendEmail($params);

		if (!Mailjet::getResponseCode() == 200){
			echo "error - ".Mailjet::getResponseCode();
			return $result;
		}
		
		return Redirect::to('/otickets');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$oticket = Oticket::findOrFail($id);
		
		//$oanswers = Oanswer::where("oticket_id",$id)->get();
		$user = Auth::user();
		$oanswers = DB::select("SELECT a.created_at, u.user_login, a.user_id, a.body, a.file FROM wp_oanswers a inner join wp_otickets t on t.id = a.oticket_id inner join wp_users u on a.user_id = u.ID where t.id = {$id} order by a.created_at asc");
		
		$viewsw = "support";
		return view('otickets.show', compact('viewsw','oticket','oanswers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
	 	$viewsw = "updates";
		$oupdate = Oupdate::findOrFail($id);
		
		return view('oupdates.edit', compact('viewsw','oupdate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$oupdate = Oupdate::findOrFail($id);
		$oupdate->title = $request->input("title");
		$oupdate->description = $request->input("description");
		$oupdate->branch = $request->input("branch");
		
		$oupdate->save();
		
		return redirect()->route('oupdates.index')->with('message', 'Update created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function destroy($id)
	{
		
		
		$oupdate = Oupdate::findOrFail($id);
		$oupdate->delete();
		
		return redirect()->route('oupdates.index')->with('message', 'Deleted successfully.');
		
	}
	
	public function store_answer(Request $request){
		
	    $dashboard_option = new DashboardOption();
		
		$user = Auth::user();
		$oanswer = new Oanswer();
		$oanswer->user_id = $user->ID;
		$oanswer->body = Input::get('body');
		$oanswer->oticket_id = Input::get('oticket_id');
		
		if($request->file('filem')!= ""){
			
			$file = $request->file('filem');
	        // SET UPLOAD PATH
	        $destinationPath = "dashboard_tickets/{$oanswer->oticket_id}";
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
	        $fileName = rand(11111, 99999) . '.' . $extension;
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$oanswer->file = $fileName;	
		}
		$oanswer->save();
		
		$oticket = Oticket::findOrFail($oanswer->oticket_id);
		$oticket->updated_at = date('Y-m-d H:i:s');
		$oticket->updated_by_login = $user->user_login;
		$oticket->save();
		
		//Mandar notificacion a suscriber
		$role = getUserRole($user->ID);
		 
		if(($role == "administrator" ) || ($role == "contributor")){
			 
			//$view = View::make('emails.ticket_answered');
			$header_image = $dashboard_option->get_meta_value("dashboard_url") . "options/" . $dashboard_option->get_meta_image("email_header_image");
			$show_more_link = $dashboard_option->get_meta_value("dashboard_url") . "otickets/" . $oticket->id;
			
			Log::info('header_image: '. $header_image);
			Log::info('show_more_link: '. $show_more_link);
			
		 	$customer =  User::findOrFail($oticket->user_id);
			$view = View::make('emails.ticket_answered', [ "ticket_id" => $oticket->id, "user_login" => $user->user_login, "answer_body" => $oanswer->body, "header_image" => $header_image, "show_more_link" => $show_more_link ]);
				
	 		$params = [
	 			"method" => "POST",
	 			"from" => $dashboard_option->get_meta_value("sender_email"),
	 			"to" => $customer->user_email,
	 			"subject" => "Your support ticket has been answered at: ".env('WP_URL'),
	 			"html" => $view->render()
	 		];
		
	 		$result = Mailjet::sendEmail($params);

	 		if (!Mailjet::getResponseCode() == 200){
	 			echo "error - ".Mailjet::getResponseCode();
	 			return $result;
	 		}
			
		 }
		
			
		return Redirect::to("/otickets/".$oanswer->oticket_id);
	}
	
	public function update_status(){
		
		$oticket = Oticket::findOrFail(Input::get('oticket_id'));
		$oticket->status = Input::get('value');
		
		$user = Auth::user();
		if($oticket->status == "CLOSED"){
			$oticket->closed_at = date('Y-m-d H:i:s');
			$oticket->closed_at_login = $user->user_login;
		}else if($oticket->status == "OPEN"){
			$oticket->closed_at = null;
			$oticket->closed_at_login = null;
		}
		
		$oticket->save();
		
		return response()->json($oticket);
	}
	
	
	
}
