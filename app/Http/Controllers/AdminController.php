<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Site;
use Input;
use App\User;
use App\Oticket;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pete;
use Response;
use Mailjet;
use Illuminate\Support\Facades\Redirect;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function __construct()
	    {
	        $this->middleware('auth.wp');		
 			$this->middleware('auth.admin');
			
	    }
	
	public function open_tickets(){
	
		
		$otickets = Oticket::where("status","OPEN")->orderBy('created_at', 'desc')->paginate(20);	
		$viewsw = "open_tickets";
		
		return view('admin.open_tickets',compact('viewsw','otickets'));
	}
	
	public function customers(){
	
		$viewsw = "customers";
		
		return view('admin.customers',compact('viewsw'));
	}
	
	public function get_customer($id){
		
		
		$wp_user = get_user_by('id',$id);
		$viewsw = "customers";
		
		//Que tenga id de usuario logeado
		$sites = Site::orderBy('id', 'desc')->where('user_id', $wp_user->ID)->get();	
		
		return view('admin.get_customer', compact('viewsw','wp_user','sites'));
			
	}
	
	public function test_email(){
		
		$params = [
			"method" => "POST",
			"from" => "software@ozonelabs.us",
			"to" => "pedroconsuegrat@gmail.com",
			"subject" => "Probando mailjet desde dashboard",
			"html" => "HELLO WORLD"
		];

		$result = Mailjet::sendEmail($params);

		if (!Mailjet::getResponseCode() == 200){
			echo "error - ".Mailjet::getResponseCode();
			return $result;
		}
		return Redirect::to('/my_subscriptions?success=yes');
	}
	
}
