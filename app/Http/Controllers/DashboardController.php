<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use DB;
use App\User;
use App\Site;
use Input;
use Illuminate\Support\Facades\Auth;
use App\Standard\Generator;
use App\Standard\License;
use Illuminate\Support\Facades\Redirect;
use App\PetePlugin;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function __construct()
	{
	      
	     $this->middleware('auth.wp');
			
	}
	
	public function plugins_json()
	{
		$pete_plugins = PetePlugin::orderBy('id', 'desc')->get();
		Log::info("plugins_json");
		
		//return response()->json($pete_plugins);
		echo Input::get('callback') . '('.json_encode($pete_plugins).')';
		
	}
	
	public function set_lang(){
		$user = Auth::user();
		
		DB::table('wp_users')->where('ID', $user->ID)->update(['dashboard_lang' => input::get('lang')]);
		return response()->json(['lang' => input::get('lang')]);
	}
	
	public function my_subscriptions(){
		
		Log::info("Entro en my_subscriptions");
		$current_user = Auth::user();
		$wp_user = get_user_by('id',$current_user->ID);
		 $viewsw = "my_subscriptions";
		
		//Que tenga id de usuario logeado
		$sites = Site::orderBy('id', 'desc')->where('user_id', $current_user->ID)->get();	
		 
		$success = Input::get('success');
		$message = Input::get('message');
		 
		return view('dashboard.my_subscriptions',compact('wp_user','sites','success','viewsw','message'));
	}
	
	
	public function create_api_key(){
		
	 	$current_user = Auth::user();
		
		$product_title = get_the_title(Input::get('product_id'));
		Log::info("Peter product title: ".$product_title);
		$sites_numbers = Site::where('user_id', $current_user->ID)->where("subscription_id",Input::get('subscription_id'))->count();
		$can_create = false;
		
		Log::info("User id: ".$current_user->ID);
		Log::info("Numero de sitios: ".$sites_numbers);
		
		Log::info("Plan: ".$product_title);
		
		if(($product_title == "Monthly License") || ($product_title == "Yearly License")){
			if($sites_numbers < 5){
				$can_create = true;
				Log::info("entro en if de 5. Starter License");
			}	
		}
		
		if($can_create == true){
			
			$site = new Site();
			$site->subscription_id = Input::get('subscription_id');
			$site->product_id = Input::get('product_id');
			$site->user_id = $current_user->ID;
			$site->user_email = $current_user->user_email;
			$site->token = bin2hex(openssl_random_pseudo_bytes(30));
			
			$site->save();
			
			$site->created_at_string = date_format($site->created_at, 'Y.m.d');
			
			//$license = new License("Juraj Puchký", "10.5.2018", "License Test", "1239", "test.devtech.cz");
			$generator = new Generator();
			$license = new License($current_user->user_email, $site->created_at_string,"WordpressPete",$site->token,"wordpresspete.com");
			$licenseKey = $generator->generate($license);
			$site->api_key = $licenseKey;
			$site->save();
				
			return Redirect::to('/my_subscriptions?success=yes');
			
		}else{
			return Redirect::to('/my_subscriptions?success=no');
		}
		
		
	}
	
	public function logout(){
		
		Auth::logout();
	    $url=wp_logout_url(env('WP_URL'));
		return redirect($url);

		//return Redirect::back();
	}
   
}
