<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
	
	//php artisan db:seed
	
    public function run()
    {
        //
		
		DB::table('pete_plugins')->truncate();
		
		DB::table('pete_plugins')->insert([
		     'title' => 'WordPress Plus Laravel',
			 'version' => "1.0",
			 'description' => 'A WordPress Laravel Integration',
			 'mac_install_script' => 'composer.phar require peteconsuegra/wordpress-plus-laravel-plugin:dev-master;
composer.phar dump-autoload;
php artisan vendor:publish --force;
php artisan addoption --option_name=wordpress_plus_laravel --option_value=/wordpress_plus_laravel --option_category=sidebar --option_privileges=all --option_order=2',
			 'olinux_install_script' => 'composer require peteconsuegra/wordpress-plus-laravel-plugin:dev-master;
composer dump-autoload;
php artisan vendor:publish --force;',
				'mac_update_script' => 'mac update script',
				'olinux_update_script' => 'olinux update script',
				'mac_uninstall_script' => 'composer.phar remove peteconsuegra/wordpress-plus-laravel-plugin;
php artisan removeoption --option_value=/wordpress_plus_laravel;
composer.phar dump-autoload',
				'olinux_uninstall_script' => 'composer remove peteconsuegra/wordpress-plus-laravel-plugin'
		]);
				
		
    }
}
