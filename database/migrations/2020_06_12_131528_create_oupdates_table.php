<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOupdatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('oupdates', function(Blueprint $table) {
            $table->increments('id');
            $table->float('parent_version');
            $table->float('version');
            $table->text('mac_script');
            $table->text('olinux_script');
            $table->text('win_script');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('oupdates');
	}

}
