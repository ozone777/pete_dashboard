<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAditionalFieldsToPlugins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pete_plugins', function (Blueprint $table) {
            //
			$table->text('uninstall_script')->nullable();
			$table->text('install_script')->nullable();
			$table->text('update_script')->nullable();
			$table->string('version')->nullable();
		
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
