<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOsScriptsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pete_plugins', function (Blueprint $table) {
            //
			$table->text('win_uninstall_script')->nullable();
			$table->text('win_install_script')->nullable();
			$table->text('win_update_script')->nullable();
			
			$table->text('mac_uninstall_script')->nullable();
			$table->text('mac_install_script')->nullable();
			$table->text('mac_update_script')->nullable();
			
			$table->text('olinux_uninstall_script')->nullable();
			$table->text('olinux_install_script')->nullable();
			$table->text('olinux_update_script')->nullable();
		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
